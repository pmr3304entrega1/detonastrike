from django.urls import path
from . import views

app_name = 'detonastrike'

urlpatterns = [
    path('', views.jogoListView.as_view(), name='index'),
    path('search/', views.search_movies, name='search'),
    path('<int:pk>/', views.jogoDetalheView.as_view(), name='detalhe'),
    path('create/', views.jogoCreateView.as_view(), name='create'),
    path('update/<int:pk>/', views.jogoUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', views.jogoDeleteView.as_view(), name='delete'),
    path('review/<int:jogo_id>/', views.jogoReviewView, name='review'),
    path('lists/', views.ListListView.as_view(), name='lists'),
    path('lists/create/', views.ListCreateView.as_view(), name='create_list'),
    path('detalist/<int:list_id>/', views.detal, name='detalist'),

    #path('<int:jg_id>/', views.detalhe, name='detalhe'),
    path('sobre', views.sobre, name='sobre'),
    #path('create', views.create_jogo, name='create'),
    #path('update/<int:jg_id>/', views.update_jogo, name='update'),
    #path('delete/<int:jg_id>/', views.delete_jogo, name='delete'),
    
]