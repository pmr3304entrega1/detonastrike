from django.db import models
from django.conf import settings


class jogo(models.Model):
    name = models.CharField(max_length=255)
    release_year = models.IntegerField()
    poster_url = models.CharField(max_length=200, null=True)
    texto = models.CharField(max_length=1000, null=True)
    texto2 = models.CharField(max_length=1000, null=True)
    texto3 = models.CharField(max_length=1000, null=True)
    texto4 = models.CharField(max_length=1000, null=True)
    imagem1 = models.CharField(max_length=200, null=True)
    imagem2 = models.CharField(max_length=200, null=True)
    imagem3 = models.CharField(max_length=200, null=True)
    imagem4 = models.CharField(max_length=200, null=True)
    imagemdecapa = models.CharField(max_length=200, null=True)
    video1 = models.CharField(max_length=200, null=True)
    video2 = models.CharField(max_length=200, null=True)
    def __str__(self):
        return f'{self.name} ({self.release_year})'



class Review(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE, null=True)
    text = models.CharField(max_length=255, null=True)
    data = models.DateTimeField(null=True)
    jg = models.ForeignKey(jogo, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f'"{self.text}""{self.data}"'



class List(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE,null=True)
    name = models.CharField(max_length=255, null=True)
    jg = models.ManyToManyField(jogo, null=True)

    def __str__(self):
        return f'{self.name} by {self.author}'