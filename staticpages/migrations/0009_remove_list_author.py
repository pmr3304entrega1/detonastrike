# Generated by Django 3.2.9 on 2021-12-06 00:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('staticpages', '0008_auto_20211205_2150'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='list',
            name='author',
        ),
    ]
