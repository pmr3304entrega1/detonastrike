# Generated by Django 3.2.9 on 2021-12-05 22:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('staticpages', '0004_review'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='data',
            field=models.DateTimeField(null=True),
        ),
    ]
