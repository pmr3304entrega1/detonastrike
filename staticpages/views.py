from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from .basededados import jg_data
from .models import jogo, Review, List
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, request
from django.urls import reverse, reverse_lazy
from django.views import generic
from datetime import date
from django.contrib.auth.decorators import login_required


def index(request):
    jogo_list = jogo.objects.all()
    context = {'jogo_list': jogo_list}
    return render(request, 'staticpages/index.html', context)

def sobre(request):
    context = {}
    return render(request, 'staticpages/sobre.html', context)


def detalhe(request, jg_id):
    jg = get_object_or_404(jogo, pk=jg_id)
    context = {'jg': jg}
    return render(request, 'staticpages/detalhe.html', context)


def search_jogo(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        jogo_list = jogo.objects.filter(name__icontains=search_term)
        context = {"jogo_list": jogo_list}
    return render(request, 'staticpages/search.html', context)

def create_jogo(request):
    if request.method == 'POST':
        name = request.POST['name']
        release_year = request.POST['release_year']
        poster_url = request.POST['poster_url']
        texto = request.POST['texto']
        texto2 = request.POST['texto2']
        texto3 = request.POST['texto3']
        texto4 = request.POST['texto4']
        imagem1 = request.POST['imagem1']
        imagem2 = request.POST['imagem2']
        imagem3 = request.POST['imagem3']
        imagem4 = request.POST['imagem4']
        imagemdecapa = request.POST['imagemdecapa']
        video1 = request.POST['video1']
        video2 = request.POST['video2']

        jg = jogo(name=name,
                      release_year=release_year,
                      poster_url=poster_url,
                      texto=texto,
                      texto2=texto2,
                      texto3=texto3,
                      texto4=texto4,
                      imagem1=imagem1,
                      imagem2=imagem2,
                      imagem3=imagem3,
                      imagem4=imagem4,
                      imagemdecapa=imagemdecapa,
                      video1=video1,
                      video2=video2)
        jg.save()
        return HttpResponseRedirect(
            reverse('detonastrike:detalhe', args=(jg.id,)))
    else:
        return render(request, 'staticpages/create.html', {})    



def update_jogo(request, jg_id):
    jg = get_object_or_404(jogo, pk=jg_id)

    if request.method == "POST":
        jg.name = request.POST['name']
        jg.release_year = request.POST['release_year']
        jg.poster_url = request.POST['poster_url']
        jg.texto = request.POST['texto']
        jg.texto2 = request.POST['texto2']
        jg.texto3 = request.POST['texto3']
        jg.texto4 = request.POST['texto4']
        jg.imagem1 = request.POST['imagem1']
        jg.imagem2 = request.POST['imagem2']
        jg.imagem3 = request.POST['imagem3']
        jg.imagem4 = request.POST['imagem4']
        jg.imagemdecapa = request.POST['imagemdecapa']
        jg.video1 = request.POST['video1']
        jg.video2 = request.POST['video2']

        jg.save()
        return HttpResponseRedirect(
            reverse('detonastrike:detalhe', args=(jg.id, )))

    context = {'jg': jg}
    return render(request, 'staticpages/update.html', context)


def delete_jogo(request, jg_id):
    jg = get_object_or_404(jogo, pk=jg_id)

    if request.method == "POST":
        jogo.delete()
        return HttpResponseRedirect(reverse('detonastrike:index'))

    context = {'jg': jg}
    return render(request, 'staticpages/delete.html', context)


def search_movies(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        jogo_list = jogo.objects.filter(name__icontains=search_term)
        context = {"jogo_list": jogo_list}
    return render(request, 'staticpages/search.html', context)


class jogoListView(generic.ListView):
    model = jogo
    template_name = 'staticpages/index.html'

class jogoDetalheView(generic.DetailView):
    model = jogo
    template_name = 'staticpages/detalhe.html'

    def get_context_data(self, **kwargs):
        id = self.kwargs['pk'] 
        context = super().get_context_data(**kwargs)
        review = Review.objects.filter(jg_id=id).order_by('-data')
        context['reviews']=review
        return context


class jogoCreateView(generic.CreateView):
    model = jogo
    fields=['name','texto','release_year','texto2','texto3','texto4','poster_url','imagem1','imagem2','imagem3','imagem4','imagemdecapa','video1','video2']
    template_name = 'staticpages/create.html'

class jogoUpdateView(generic.UpdateView):
    model = jogo
    fields=['name','texto','release_year','texto2','texto3','texto4','poster_url','imagem1','imagem2','imagem3','imagem4','imagemdecapa','video1','video2']
    template_name = 'staticpages/update.html'

class jogoDeleteView(generic.DeleteView):
    model = jogo
    template_name = 'staticpages/delete.html'


@login_required
def jogoReviewView(request, jogo_id):
    jg = get_object_or_404(jogo, pk=jogo_id)
    if request.method == 'POST':
            
            author = request.user 
            texto = request.POST['text']
            data = date.today()
            rev = Review(author=author,
                            text=texto,
                            data=data,
                            jg = jg)
            rev.save()
            return HttpResponseRedirect(
                reverse('detonastrike:detalhe', args=(jg.id, )))
    else:
        context = {'jogo': jg}

    return render(request, 'staticpages/review.html', context)

#parte 3

class ListListView(generic.ListView):
    model = List
    template_name = 'staticpages/lists.html'


class ListCreateView(generic.CreateView):
    model = List
    template_name = 'staticpages/create_list.html'
    fields = ['name', 'author', 'jg']
    success_url = reverse_lazy('detonastrike:lists')

def detal(request, list_id):
    det = get_object_or_404(List, pk=list_id)
    context = {'lista': det}
    return render(request, 'staticpages/detalist.html', context)