jg_data=[{
    "id":
    "1",
    "name":
    "FIFA",
    "texto":
    """Em 15 de julho de 1993, a Electronic Arts usava o selo EA Sports para apresentar ao mundo o seu mais novo jogo: FIFA International Soccer. Lançado na época para nada menos do que 10 plataformas diferentes, o game era a aposta da EA para o esporte mais popular do planeta, visto que a empresa já tinha há alguns anos títulos de futebol americano, golfe, hóquei e basquete.

De lá para cá, a franquia passou por altos e baixos, mas se estabeleceu como a principal franquia esportiva do mundo, superando rivais que já não existem mais e nomes de peso, como a série Pro Evolution Soccer/Winning Eleven, da KONAMI. Você é fã de futebol? Curte relembrar da história dos games? Então relembre agora todas as 24 capas globais de FIFA.

Hoje faremos um comparativo da aparência de jogadores em campo e dos estádios, do FIFA de 2002 e de 2022, evidenciando as principais mudanças ao longo desses 20 anos.

Jogagores da seleção brasileira, comemorando o título da copa do mundo:""",
    "imagem1":
    "/static/jogadoresemcampo960.jpg",

    "texto2":
    """Uma cena entre Real Madrid e PSG, no FIFA 22:""",

    "imagem2":
    "/static/jogadores2022.png",

    "texto3":
    """No decorrer desse tempo, ficou claro que os detalhes alcançados pela tecnologia foram gigantescos, dando aos players uma maior aproximação da realidade, como no comparativo agora dos estádios, dêem uma olhada:""",
    
    "imagem3":
    "/static/estadio2002certo960.jpg",

    "texto4":
    """Reparem na quantidade de detalhes que foram implementdos no jogo em 2002 e agora em 2022, a diferença é tão alta que realmente a foto se aproxima da realidade!""",
    
    "imagem4":
    "/static/estadio22c960.png",

    "imagemdecapa":
    "/static/fifa.jpg",


},{
    "id":
    "2",
    "name":
    "Counter Strike Global Offensive",

    "texto":
    """A última versão do Counter-Strike nasceu em 2012, com o subtítulo de Global Offensive. Em uma época com muito mais recursos, a Valve lançou um CS que finalmente “aposentou” o 1.6. Mais do que isso, parece ter dado um novo ânimo ao mundo das competições.

O novo jogo conquistou muitos novos fãs, que agora já têm um acesso mais fácil à PCs e internet banda larga. Com novos fãs, chegaram novos times, novos jogadores, novos campeonatos e tudo isso favoreceu para que o cenário brasileiro e mundial se fortalecesse ainda mais.

Com isso, o CS:GO se tornou o jogo estilo FPS mais jogado da atualidade. Ele possui muitas estratégias interessantes, como o uso as smokes, flashs, molotov, etc.

Vamos aprender um pouco sobre as utilitárias usadas na Mirage e Nuke, mapas que fazem parte do cenário competitivo do CS:GO.

Primeiro, é importante lembrar que se deve contar com o comando do jump throw, utilizado normalmente na letra "n". Para isso, basta abrir o console e digitar: bind n "+jump;-attack;-jump".

A começar pelo mapa da Mirage:

(abra os vídeos no link)""",

    "video1":
    "https://www.youtube.com/watch?v=-jqZHOsJUXQ",

    "imagem1":
    "/static/mirage.jpg",

    "texto2":
    """Agora que vocês estão treinados na Mirage, aproveitem para melhorar suas habilidade na Nuke!!!""",

    "video2":
    "https://www.youtube.com/watch?v=5ixW3jKbhP0",

     "imagem2":
    "/static/nuke.jpg",

    "imagemdecapa":
    "/static/csgo.jpg",

},{

    "id":
    "3",
    "name":
    "GTA V",

    "texto":
    """Quando um malandro de rua, um ladrão de bancos aposentado e um psicopata aterrorizante se envolvem com alguns dos criminosos mais assustadores e loucos do submundo, o governo dos EUA e a indústria do entretenimento, eles devem realizar golpes ousados para sobreviver nessa cidade implacável onde não podem confiar em ninguém, nem mesmo um no outro.

É meus amigos, esse é o grande GTA V, o maior jogo desenvolvido pela Rockstar Games foi desenvolvido por incríveis 265 milhões de dólares. Foi lançado no ano de 2013, ainda em 2022 é muito jogado por todo o mundo.

Venham conhecer alguns easter eggs sobre esse jogo que esconde centenas de segredos, assim como diversos jogos da franquia! """,

    "texto2":
    """Eu vejo pessoas mortas: o fantasma de Jolene:
    
    Jolene Cranley Evans foi assassinada pelo marido, Jock, ao ser jogada do Monte Gordo em 1978. O assassino, no entanto, nunca foi preso por “falta de provas”.
Só é possível encontrar o fantasma entre às 23h e a meia-noite. Como o espírito desaparece se chegar muito perto, use um rifle sniper para observá-la pela mira ou a câmera do celular.
Jolene aparece numa rocha do Monte Gordo. Quando ela desaparece, você pode ver uma mensagem escrita no chão, em sangue: “Jock”. Em versões mais otimizadas do jogo, é possível ouvir gritos e outros sons sinistros vindos do local.""",

    "imagem1":
    "/static/fantasma.PNG",

    "texto3":
    """Red Dead Redemption:
    
    Você pode encontrar uma referência a Red Dead Redemption (também da Rockstar) em Strawberry. Abaixo de uma rodovia, há vários grafites na parede. Ao olhar com atenção é possível encontrar a silhueta de John Marston (protagonista de RDR) montado num cavalo sob a luz do luar. Poético.""",

    "imagem2":
    "/static/reddead.PNG",

    "texto4":
    """Zumbi faminto (mas “gente” boa):

    Vá até West Vinewood, uma clara referência a Hollywood. Por lá, o jogador encontrará algo que se assemelha a Calçada da Fama – com direito a estrelas desenhadas no chão e tudo.O mais curioso é que, andando por este local, é bem capaz de você trombar com um zumbi. Não alguém vestido de zumbi, mas um de “verdade”. Só que ao invés dele te atacar, ele começa a ter uma conversa totalmente doida com você sobre a necessidade de lobotomias obrigatórias e os tipos de cérebros que ele queria comer.""",

    "imagem3":
    "/static/zumbi.PNG",

    "imagemdecapa":
    "/static/gta.jpg",
}]